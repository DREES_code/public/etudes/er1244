# ER1244

Ce dossier contient le programme ayant servi à produire [l'*Études et résultats* n°1244 "Quatre médecins généralistes sur dix exercent dans un cabinet pluriprofessionnel en 2022"](https://drees.solidarites-sante.gouv.fr/publications-communique-de-presse/etudes-et-resultats/quatre-medecins-generalistes-sur-dix-exercent), publié en octobre 2022.

[Présentation de la DREES](https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees) : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.

[Source de données](https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/00-le-panel-dobservation-des-pratiques-et-des-conditions-dexercice-en) : DREES, Observatoires régionaux de la santé (ORS) et Unions régionales des professions de santé (URPS) de Provence-Alpes-Côte d'Azur et des Pays de la Loire, quatrième Panel d'observation des pratiques et des conditions d'exercice en médecine générale de ville, 2018-2022.
